#!/bin/bash

# ------------------------------------------
# Antrax software server general information

# Aleksei Shentiakov
# a.shentiakov@flamesgroup.com

# last update 05.10.2017
# ------------------------------------------

INVERT="\e[7m"
CYAN="\e[36m"
RED="\e[31m"
GREEN="\e[32m"
YELLOW="\e[33m"
BLUE="\e[34m"
MAGENTA="\e[35m"
LIGHT_YELLOW="\e[93m"
RESET_COLOR="\e[0m"

CONF_FILES_COLOR=$CYAN
PACKAGES_COLOR=$GREEN
JAVA_COLOR=$MAGENTA
HOSTS_COLOR=$YELLOW
DELIMITER_COLOR=$INVERT

REDHAT_COLOR=$RED
DEBIAN_COLOR=$LIGHT_YELLOW

distro_version='unknown'

commons_package_installed=0
control_server_package_installed=0
voice_server_package_installed=0
sim_server_package_installed=0
tools_package_installed=0


_delimiter () {
echo -en "$DELIMITER_COLOR"
echo -e "\n*************************\n"
echo -en "$RESET_COLOR"
}

# ------------------------

get_distro_version () {
local version=$(cat /proc/version)

if [[ "$version" =~ 'Red Hat' ]]
then
	distro_version="redhat"
	distro_color=$REDHAT_COLOR
elif [[ "$version" =~ 'debian' ]]
then
	distro_version="debian"
	distro_color=$DEBIAN_COLOR
elif [[ "$version" =~ 'Ubuntu/Linaro' ]]	# box
then
	distro_version="debian"
	distro_color=$DEBIAN_COLOR
else
	distro_version="unknown"
fi

echo ""

echo -en "distro "

echo -en "$distro_color"
echo -e "$distro_version"
echo -en "$RESET_COLOR"

echo ""

# release info
cat /etc/*release

_delimiter
}

# ------------------------

get_installed_antrax_packages () {

echo -en "$PACKAGES_COLOR"
echo -e "installed antrax packages"
echo -en "$RESET_COLOR"

echo ""

local installed_antrax_packages

if [ $distro_version = 'redhat' ]
then
# red hat distro
installed_antrax_packages=$(yum list installed antrax.* | grep 'antrax.')
elif [ $distro_version = 'debian' ]
then
# debian distro
installed_antrax_packages=$(apt list --installed antrax.* | grep 'antrax.')
elif [ $distro_version = 'unknown' ]
then
echo 'unknown linux distro!'
exit 1
fi

# check if antrax commons package installed
echo "$installed_antrax_packages" | grep -q 'antrax.commons.'

if [ $? -eq 0 ]
then
commons_package_installed=1
echo "$installed_antrax_packages" | grep 'antrax.commons.'
else
commons_package_installed=0
fi

# check if antrax control server package installed
echo "$installed_antrax_packages" | grep -q 'antrax.control-server.'

if [ $? -eq 0 ]
then
control_server_package_installed=1
echo "$installed_antrax_packages" | grep 'antrax.control-server.'
else
control_server_package_installed=0
fi

# check if antrax voice server package installed
echo "$installed_antrax_packages" | grep -q 'antrax.voice-server.'

if [ $? -eq 0 ]
then
voice_server_package_installed=1
echo "$installed_antrax_packages" | grep 'antrax.voice-server.'
else
voice_server_package_installed=0
fi

# check if antrax sim server package installed
echo "$installed_antrax_packages" | grep -q 'antrax.sim-server.'

if [ $? -eq 0 ]
then
sim_server_package_installed=1
echo "$installed_antrax_packages" | grep 'antrax.sim-server.'
else
sim_server_package_installed=0
fi

# check if antrax tools package installed
echo "$installed_antrax_packages" | grep -q 'antrax.tools.'

if [ $? -eq 0 ]
then
tools_package_installed=1
echo "$installed_antrax_packages" | grep 'antrax.tools.'
else
tools_package_installed=0
fi

_delimiter
}

# ------------------------

get_java () {
echo -en "$JAVA_COLOR"
echo -e "java"
echo -en "$RESET_COLOR"

echo ""

java -version &> /dev/null

if [ $? -eq 127 ]
then
echo "java is not installed"
else
java -version 2>&1
fi

echo ""

echo "JAVA_HOME = $JAVA_HOME"

_delimiter
}

# ------------------------

get_antrax_config () {
	
# commons
local config_server_name='/opt/antrax/commons/etc/server.name'
local server_name

local config_control_server_url='/opt/antrax/commons/etc/control.server.url'
local control_server_url

local config_commons_properties='/opt/antrax/commons/etc/commons.properties'
local cs_max_allowable_time_difference

if [ $commons_package_installed -eq 1 ]
then
server_name=$(cat ${config_server_name})

echo -en "$CONF_FILES_COLOR"
echo -e "$config_server_name"
echo -en "$RESET_COLOR"

echo -e "$server_name\n"

control_server_url=$(cat $config_control_server_url)

echo -en "$CONF_FILES_COLOR"
echo -e "$config_control_server_url"
echo -en "$RESET_COLOR"

echo -e "$control_server_url\n"

cs_max_allowable_time_difference=$(cat ${config_commons_properties} | grep 'time.difference')

echo -en "$CONF_FILES_COLOR"
echo -e "$config_commons_properties"
echo -en "$RESET_COLOR"

echo -e "$cs_max_allowable_time_difference\n"
fi

# control server
local config_cs_properties='/opt/antrax/control-server/etc/control-server.properties'
local rmi_server_hostname

if [ $control_server_package_installed -eq 1 ]
then
rmi_server_hostname=$(cat ${config_cs_properties} | grep 'rmi.server.hostname')

echo -en "$CONF_FILES_COLOR"
echo -e "$config_cs_properties"
echo -en "$RESET_COLOR"

echo -e "$rmi_server_hostname\n"
fi

# voice server
local config_vs_network_properties='/opt/antrax/voice-server/etc/network.properties'
local vs_network_properties
local config_vs_devices_properties='/opt/antrax/voice-server/etc/devices.properties'
local vs_devices_properties

if [ $voice_server_package_installed -eq 1 ]
then
vs_network_properties=$(cat ${config_vs_network_properties})

echo -en "$CONF_FILES_COLOR"
echo -e "$config_vs_network_properties"
echo -en "$RESET_COLOR"

echo -e "$vs_network_properties\n"

vs_devices_properties=$(cat ${config_vs_devices_properties})

echo -en "$CONF_FILES_COLOR"
echo -e "$config_vs_devices_properties"
echo -en "$RESET_COLOR"

echo -e "$vs_devices_properties\n"
fi

# sim server
local config_ss_network_properties='/opt/antrax/sim-server/etc/network.properties'
local ss_network_properties
local config_ss_devices_properties='/opt/antrax/sim-server/etc/devices.properties'
local ss_devices_properties

if [ $sim_server_package_installed -eq 1 ]
then
ss_network_properties=$(cat ${config_ss_network_properties})

echo -en "$CONF_FILES_COLOR"
echo -e "$config_ss_network_properties"
echo -en "$RESET_COLOR"

echo -e "$ss_network_properties\n"

ss_devices_properties=$(cat ${config_ss_devices_properties})

echo -en "$CONF_FILES_COLOR"
echo -e "$config_ss_devices_properties"
echo -en "$RESET_COLOR"

echo -e "$ss_devices_properties\n"
fi

_delimiter
}

# ------------------------

get_hosts () {
echo -en "$HOSTS_COLOR"
echo -e "/etc/hosts"
echo -en "$RESET_COLOR"

echo ""

cat '/etc/hosts'

_delimiter
}

# ------------------------

main_func () {

# linux distro
get_distro_version

# java version
get_java

# installed antrax packages
get_installed_antrax_packages

# antrax config
get_antrax_config

# hosts
get_hosts
}

# ========================

main_func




